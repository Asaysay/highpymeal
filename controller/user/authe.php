<?php // on verifie que la session existe
    function is_connected(): bool{
        if(session_status()===PHP_SESSION_NONE){
            session_start(); // on la demarre
        }

        return !empty($_SESSION['connecte']); // on renvoie la connexion existante
    }


    function forced_user_connect() : void{
        if(!is_connected()){
            header('Location: ../front/log.php');
            exit();
        }
    }



    function check_user($email,$password){
        
        require_once "../api/config/database.php";
        $database = new DatabaseService();
        $connect = $database ->getConnection();

        $query = "SELECT * FROM user_ WHERE email = :email";

        $stmt = $connect->prepare($query);

        $email=htmlspecialchars(strip_tags($email));
        $password=htmlspecialchars(strip_tags($password));

        $stmt->bindValue(':email', $email,PDO::PARAM_STR);
      

        $stmt->execute(); 

        $num = $stmt->rowCount();
        if($num > 0){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $mail = $row['email'];   
            $password2= $row['password']; 
            $actif = $row['actif'];

            var_dump($row);
          
     
            if($actif === "1"){

                if(password_verify($password, $password2)){
                    var_dump($row);
                    return $row;
                
                }else{
                     return null;
                }
            }else{
                return null;
            }
    }
}

?>