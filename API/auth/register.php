<?php
include_once '../config/database.php';


header("Access-Control-Allow-Origin: * "); // tout le monde a acces a l'api
header("Access-Control-Allow-Methods: POST"); //methode post
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD']=='POST'){

    $lastName = '';
    $firstName = '';
    $date_naissance = '';
    $premium = false;
    $admin = false;
    $password = '';
    $email = '';
    $conn = null;

    $databaseService = new DatabaseService();
    $conn = $databaseService->getConnection();



    $data = json_decode(file_get_contents("php://input"));


    // $lastName = $data -> nom;
    // $firstName =$data -> prenom;
    // $date_naissance =$data -> date_naissance;
    // $premium =$data -> premium;
    // $admin =$data -> admin;
    // $password =$data -> password;
    // $email =$data -> email;


    $lastName = $_POST['nom'];
    $firstName =$_POST['prenom'];
    $date_naissance =$_POST['date_naissance'];
    $password =$_POST['password'];
    $email =$_POST['email'];

    $table_name = 'user_';

    $query = "INSERT INTO user_
                    SET
                        nom = :lastName,
                        prenom = :firstName,
                        date_naissance = :date_naissance,
                        premium = :premium,
                        admin = :admin,
                        password = :password,
                        email = :email,
                        actif = false";


   
    $stmt = $conn->prepare($query);

    $lastName=htmlspecialchars(strip_tags($lastName));
    $firstName=htmlspecialchars(strip_tags($firstName));
    $date_naissance=htmlspecialchars(strip_tags($date_naissance));
    $password=htmlspecialchars(strip_tags($password));
    $email=htmlspecialchars(strip_tags($email));
   

    $stmt->bindValue(':lastName', $lastName,PDO::PARAM_STR);
    $stmt->bindValue(':firstName', $firstName,PDO::PARAM_STR);
    $stmt->bindValue(':date_naissance', $date_naissance,PDO::PARAM_STR);
    $stmt->bindValue(':premium', $premium,PDO::PARAM_BOOL);
    $stmt->bindValue(':admin', $admin,PDO::PARAM_BOOL);

    $password_hash = password_hash($password, PASSWORD_BCRYPT);

    $stmt->bindValue(':password',$password_hash,PDO::PARAM_STR);

    $stmt->bindValue(':email', $email,PDO::PARAM_STR);



    if($stmt->execute()){
        
        
        http_response_code(200);
        header('Location: http://localhost/HighpyMeal/front/SignOk.php');
        
    }
    else{
    
    
        http_response_code(400);

        echo json_encode(array("message" => "Impossible de crééer l'utilisateur."));
    }
}else{
    http_response_code(405);
    echo json_encode(["message"=>"La méthode n'est pas bonne"]);
}
?>