<?php
// Se connecter à la base de données
  include("db_connect.php"); 


$request_method = $_SERVER["REQUEST_METHOD"];


  switch($request_method)
  {
    case 'GET':

      if(!empty($_GET["id"]))
      {
        // Récupérer une seule recette
        $id = intval($_GET["id"]);
        getIdsQuantiteIngredientsRecette($id);
      }
      else
      {
        // id manquant
        echo ("veuillez renseigner l'id de la recette");
      }
      break;
    default:
      // Requête invalide
      header("HTTP/1.0 405 Method Not Allowed");
      break;
  }

  function getIdsQuantiteIngredientsRecette($id=0)
  {
    global $conn;
    $query = "SELECT * FROM `ingredient_recette` WHERE `id_recette` =".$id;
    // $query = "SELECT * FROM `ingredient_recette`";
    // if($id != 0)
    // {
    //   $query .= " WHERE id_recette=".$id;
    // }
    $response = array();
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result))
    {
      $response = $row;
    }


    // var_dump($response);

    header('Access-Control-Allow-Origin: *');
    // header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    // header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
    header('Access-Control-Allow-Credentials: true');
  
    header('Content-Type: application/json');

      echo json_encode($response, JSON_PRETTY_PRINT);
      // var_dump ($response);
  }

  function getRecettes()
  {
    global $conn;
    $query = "SELECT * FROM recette";
    $response = array();
    $result = mysqli_query($conn, $query);

    while($row = mysqli_fetch_array($result))
    {
      $response[] = $row;
    }
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
    // var_dump ($response);
  }

  function getIngredientsRecette($id=0){
    global $conn;
    $query = "SELECT * FROM `ingredient_recette` WHERE `id_recette` =".$id;
    $response = array();
    $result = mysqli_query($conn, $query);

    while($row = mysqli_fetch_array($result))
    {
      $response[] = $row;
    }
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);

  }


?>