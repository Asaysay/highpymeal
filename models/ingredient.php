<?php
require_once "../api/config/database.php";
class Ingredient{

    public $connect;


    public function get_db(){
        $db = new DatabaseService();
        $con = $db -> getConnection();
        return $con;
    }


    public function get_da_one($id_fam){

        if (empty($id_fam)){

            return false;

        }else{

            $connect = $this->get_db();
            $query = "SELECT * FROM ingredient WHERE id_famille = :id_famille";

            $stmt = $connect->prepare($query);

            
            $stmt->bindValue(':id_famille', $id_fam,PDO::PARAM_INT);
            
            if($stmt->execute()){
              

                return $stmt;    
            }else{
                return false;
            }

        }
    }



}


?>