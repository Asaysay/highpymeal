<?php
include_once '../api/config/database.php';  
class Famille {

    public $conn;  


    public function Connect_db(){

        $database = new DatabaseService();
        $connect = $database ->getConnection();

        return $connect;
    }



    public function get_all(){

        $conn = $this->Connect_db();
        $query = "SELECT * FROM famille";

        $stmt = $conn->prepare($query);
        $stmt->execute(); 

        return $stmt;    

    }
          
 }

       
?>