<?php
// include_once '../api/config/database.php';
include_once '../API/config/database.php';  
class Recettes {

    public $conn;  


    public function Connect_db(){

        $database = new DatabaseService();
        $connect = $database ->getConnection();

        return $connect;
    }
    
    public function lire(){

        $conn = $this->Connect_db();
        $query = "SELECT * FROM recette";

        $stmt = $conn->prepare($query);
        
        if($stmt->execute()){
            return $stmt;    

        }


    }


    public function lire_rec_user($iduser){

        $conn = $this->Connect_db();
        
        $query = "SELECT * FROM recette WHERE id_user = :id";

        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $iduser,PDO::PARAM_INT);
        
        if($stmt->execute()){

            return $stmt;    
        }else{
            return false;
        }


    }

    public function lire_recette_selected_user($idrecette){

        $conn = $this->Connect_db();

        $query = "SELECT `id_recette`, `nom`, `descriptif`, `temps_preparation`, `Nombre_de_personnes`, `Temps_cuisson`, `id_user` FROM `recette` 
                  WHERE `id_recette`=:id";
                  
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $idrecette,PDO::PARAM_INT);
        
        if($stmt->execute()){
            return $stmt;    
        }else{
            return false;
        }
    }

    public function lire_ingredients_recette_selected($idrecette){
        $conn = $this->Connect_db();

        $query = "SELECT * FROM `ingredient_recette` WHERE `id_recette` =:id";
                  
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $idrecette,PDO::PARAM_INT);
        
        if($stmt->execute()){
            return $stmt;    
        }else{
            return false;
        }
    
    }

    public function lire_nom_ingredient_recette($id_ingredient){
        $conn = $this->Connect_db();

        $query = "SELECT `nom` FROM `ingredient` WHERE `id_ingredient` =:id";
                  
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $id_ingredient,PDO::PARAM_INT);
        
        if($stmt->execute()){
            return $stmt;    
        }else{
            return false;
        }
    
    }


    public function GetImageRecette($idrecette){

        $conn = $this->Connect_db();
        
        $query = "SELECT * FROM image WHERE id_recette = :id";

        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $idrecette,PDO::PARAM_INT);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        if($stmt->execute()){
            $tab = $stmt->fetchAll();
            if(!empty($tab[0]["bin"])){

                return $tab[0]["bin"];    
            }else{
                return true;
            }
        }else{
            return false;
        }

    }


    public function SavePicture($nom,$taille,$type,$bin){
      
        if(!empty($nom) && !empty($taille) && !empty($bin) && !empty($type)){

            $lastid = $this->getLastId();
            $conn = $this->Connect_db();
    
            $query = "INSERT INTO image (nom,taille,type,bin,id_recette) 
                    VALUES (:nom,:taille,:type,:bin,:idrecette)";  
    
            $etat = $conn->prepare($query);
            $etat->bindValue(':nom', $nom,PDO::PARAM_STR);
            $etat->bindValue(':taille', $taille,PDO::PARAM_INT);
            $etat->bindValue(':type', $type,PDO::PARAM_STR);
            $etat->bindValue(':bin', $bin,PDO::PARAM_STR);
            $etat->bindValue(':idrecette', $lastid,PDO::PARAM_INT);
    
            if($etat->execute()){
                echo ("photo enreg");
                return true;
            } 
        }else{
            return false;
        }

    }


    public function lire_one($idRecette=null){
  

        $conn = $this->Connect_db();

        $query = "SELECT * FROM recette LEFT JOIN user_ ON recette.id_user = user_.id_user LIMIT 6";

        if($idRecette !== null){

            $query.=" WHERE id_recette = :idrecette"; 
            $etat->bindValue(':idrecette', $idRecette,PDO::PARAM_INT);

        }
        
        $etat = $conn->prepare($query);

        $etat->execute(); 

        return $etat;    

    }


    public function Add_recette($nom,$descr,$tps_prepa,$Nb,$tps_cui,$id){
  

        $conn = $this->Connect_db();

        $query = "INSERT INTO recette (nom, descriptif, temps_preparation, Nombre_de_personnes, Temps_cuisson,id_user) 
                VALUES (:nom,:descriptif,:temps_preparation,:Nombre_de_personnes,:Temps_cuisson,:id_user)";  

        $etat = $conn->prepare($query);

        $nom=htmlspecialchars(strip_tags($nom));
        $descr=htmlspecialchars(strip_tags($descr));
        $tps_prepa=htmlspecialchars(strip_tags($tps_prepa));
        $Nb=htmlspecialchars(strip_tags($Nb));
        $tps_cui=htmlspecialchars(strip_tags($tps_cui));
        $id=htmlspecialchars(strip_tags($id));     

        $etat = $conn->prepare($query);

        $etat->bindValue(':nom', $nom,PDO::PARAM_STR);
        $etat->bindValue(':descriptif', $descr,PDO::PARAM_STR);
        $etat->bindValue(':temps_preparation', $tps_prepa,PDO::PARAM_STR);
        $etat->bindValue(':Nombre_de_personnes', $Nb,PDO::PARAM_INT);
        $etat->bindValue(':Temps_cuisson', $tps_cui,PDO::PARAM_STR);
        $etat->bindValue(':id_user', $id,PDO::PARAM_INT);
       
       
       if($etat->execute()){
           
           return true;
       } 
       
       else{
            echo("recette ne s'execute pas");
        };

    }


    public function SupprimerRecette($id){
        
        $conn = $this->Connect_db();

        $query = "DELETE FROM recette WHERE id_recette = :id ";

    
        $etat = $conn->prepare($query);
        
        $etat->bindValue(':id', $id,PDO::PARAM_INT);
        if($etat->execute()==true){

                 return true;
            } 
        } 

        public function SupprimerIngredient($id_recette){
        
            $conn = $this->Connect_db();
    
            $query = "DELETE FROM ingredient_recette WHERE id_recette = :id ";
    
        
            $etat = $conn->prepare($query);
            
            $etat->bindValue(':id', $id_recette,PDO::PARAM_INT);
            if($etat->execute()==true){
    
                     return true;
                } 
            }     


            public function Modifier_recette($id_recette,$nom,$descr,$tps_prepa,$Nb,$tps_cui){
        
                $conn = $this->Connect_db();
                
                $query = "UPDATE recette
                SET nom = :nom,
                  descriptif = :descriptif,
                  temps_preparation = :temps_preparation,
                  Nombre_de_personnes = :Nombre_de_personnes,
                  Temps_cuisson = :Temps_cuisson,
               
                WHERE id_recette = :id_recette";
        
            
                $etat = $conn->prepare($query);
                
                
                $nom=htmlspecialchars(strip_tags($nom));
                $descr=htmlspecialchars(strip_tags($descr));
                $tps_prepa=htmlspecialchars(strip_tags($tps_prepa));
                $Nb=htmlspecialchars(strip_tags($Nb));
                $tps_cui=htmlspecialchars(strip_tags($tps_cui));
                
                $id=htmlspecialchars(strip_tags($id_recette));


                $etat->bindValue(':nom', $nom,PDO::PARAM_STR);
                $etat->bindValue(':descriptif', $descr,PDO::PARAM_STR);
                $etat->bindValue(':temps_preparation', $tps_prepa,PDO::PARAM_INT);
                $etat->bindValue(':Nombre_de_personnes', $Nb,PDO::PARAM_INT);
                $etat->bindValue(':Temps_cuisson', $tps_cui,PDO::PARAM_INT);
             
                $etat->bindValue(':id_recette', $id_recette,PDO::PARAM_INT);

                if($etat->execute()==true){
        
                         return true;
                    } 
                }  
                
                
        public function JoinIngredientRecette($ids_ingredients){
            
            $lastId = null;
    
            $conn = $this->Connect_db();
    
            $lastId =$this->getLastId();
    
    
            for($i= 0; $i < count($ids_ingredients); $i++){
                $query = "INSERT INTO ingredient_recette (gramme,quantite,id_ingredient,id_recette) 
                            VALUES (:gramme,:quantite,:id_ingredient,:lastId)";
                $etat = $conn->prepare($query);
    
                $etat->bindValue(':lastId', $lastId,PDO::PARAM_INT);
                $etat->bindValue(':id_ingredient', $ids_ingredients[$i][0],PDO::PARAM_INT);
                $etat->bindValue(':gramme', $ids_ingredients[$i][1],PDO::PARAM_INT);
                $etat->bindValue(':quantite', $ids_ingredients[$i][2],PDO::PARAM_INT);
                if($etat->execute()){   
                    echo('aliment ajouté');             
                }  
            }
            
            return $lastId;
           
    
        }


        public function getLastId(){

            $lastId = null;
    
            $conn = $this->Connect_db();
    
            // recupere dernier id_recette
            $queryGetDernierIdRecette = "SELECT MAX(id_recette) FROM recette";
    
            $etat = $conn->prepare($queryGetDernierIdRecette);
    
            $etat->execute(); 
    
            if(!$etat->execute()){
                echo('Erreur');
                return false;
            }
    
            $num = $etat->rowCount();
    
            if($num > 0){
            $row = $etat->fetch(PDO::FETCH_ASSOC);
            $lastId = $row["MAX(id_recette)"];
            return $lastId;
            }

        }
            
    }



    

       
?>