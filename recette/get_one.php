<?php
// include_once '../api/config/database.php';


if($_SERVER['REQUEST_METHOD']=='GET'){

    include_once '../models/recette.php';

    $data = json_decode(file_get_contents("php://input"));
    
    $recettes = new Recettes();

    if ($data !== null){

        $id = $data->id_recette;

    }else{
        $id = null;
    }

    $stmt = $recettes->lire_one($id);
   
    $num = $stmt->rowCount();
  
    if ($num > 0){
        $tabRecette=[];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

            extract($row);
             
           $produit = [
               "id_recette" => $id_recette,
               "nom" => $nom,
               "descriptif"=>$descriptif,
               "temps_preparation"=>$temps_preparation,
               "Nombre_de_personnes"=>$Nombre_de_personnes,
               "Temps_cuisson"=>$Temps_cuisson,
               "id_user"=>$id_user
           ];
           
           array_push($tabRecette,$produit);
        }
        
        http_response_code(200);  
        // echo json_encode($tabRecette);
        
    }
    

}else{
    http_response_code(405);
    echo json_encode(["message"=>"La méthode n'est pas bonne"]);
}

?>
