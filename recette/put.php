<?php
// include_once '../api/config/database.php';


header("Access-Control-Allow-Origin: * "); // tout le monde a acces a l'api
header("Content-Type: application/json; charset=UTF-8"); // reponse en json
header("Access-Control-Allow-Methods: PUT"); //methode post
header("Access-Control-Max-Age: 3600"); // durée de vie de la requete
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD']=='PUT'){

    include_once '../models/recette.php';

    $data = json_decode(file_get_contents("php://input"));
    
    $recettes = new Recettes();

    $id_recette = $data->id_recette;
    $nom = $data->nom;
    $descriptif = $data->descriptif;
    $temps_preparation = $data->temps_preparation;
    $Nombre_de_personnes = $data->Nombre_de_personnes;
    $Temps_cuisson = $data->Temps_cuisson;
    $photo = $data->photo;
  

    if (!empty($id_recette)){

    $stmt = $recettes->Modifier_recette($id_recette,$nom,$descriptif,$temps_preparation,$Nombre_de_personnes,$Temps_cuisson,$photo);

    if($stmt===true){

        echo "La recette a bien été modifié";
        http_response_code(200);    
    }

  }else{
      echo "Veuillez sélectionner une recette";
  }
    

}else{
    http_response_code(405);
    echo json_encode(["message"=>"La méthode n'est pas bonne"]);
}

?>
