<?php

header("Access-Control-Allow-Origin: * "); // tout le monde a acces a l'api
header("Content-Type: application/json; charset=UTF-8"); // reponse en json
header("Access-Control-Allow-Methods: POST"); //methode post
header("Access-Control-Max-Age: 3600"); // durée de vie de la requete
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD']=='POST'){

    include_once '../models/recette.php';

   // $data = file_get_contents("php://input");
   
    $recettes = new Recettes();
   
    $nom = $_POST['tab2'][0];
    $descriptif = $_POST['tab2'][1];
    $temps_preparation = $_POST['tab2'][2];
    $Nombre_de_personnes = $_POST['tab2'][3];
    $Temps_cuisson = null;
    $id_user = $_POST['tab2'][4];
    

    if (!empty($nom) && !empty($descriptif) && !empty($temps_preparation) 
    && !empty($Nombre_de_personnes) && !empty($id_user)){

    $stmt = $recettes->Add_recette($nom,$descriptif,$temps_preparation,$Nombre_de_personnes,$Temps_cuisson,$id_user);
   
    if($stmt===true){
        
        http_response_code(200);  
        
        $aliment = $recettes->JoinIngredientRecette($_POST['tab1']);
        

        if($aliment!==0){

          
        echo "La recette est bien enregistrée";
        return true;
               
        }
    }

  }else{
      echo "Veuillez remplir tous les champs s'il vous plaît";
      exit();
  }
    

}else{
    http_response_code(405);
    echo json_encode(["message"=>"La méthode n'est pas bonne"]);
}

?>
