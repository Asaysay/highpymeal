<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Highpy Meal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="css/accueil1.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">
    
    <!-- Bootstrap core CSS -->
    <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    
     <link href="css/cree.css" rel="stylesheet">
    <meta name="theme-color" content="#7952b3">

    </head>
    
    <body class = "bod">
    <?php
        include("header.php");
    ?>

    <?php if(!empty($_FILES["image"])){
       
        require_once "../models/recette.php";
        require_once "../api/config/database.php";
        $conn = null;
        $recette = new Recettes();

        $db = new DatabaseService();

        $conn= $db-> getConnection();
        $req =$recette->SavePicture($_FILES["image"]["name"],$_FILES["image"]["size"],$_FILES["image"]["type"],file_get_contents($_FILES["image"]["tmp_name"]));


    }?>
        <div class="enzo">
           <div class="crt">
               <h1 class="fw-light">Atelier préparation</h1>
               <p class="fw-light">Poste ta recette</p>
               <hr class ="hh">
           </div>
           <h2  class="fw-light bde">Information sur ta recette</h2>
            </br>
           <div id="prepa">
           <form>
                <div class="form-group row">
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" name ="nom" placeholder="Nom de la recette">
                    </div>
                </div>
            </br>   
                <div class ="tps">
                    <div class="col">
                     <input type="number" class ="inpt" id="tpps" name ="temps_prepa" placeholder="Temps de preparation en minutes.." min ="0" max ="1000">
                    </div>
                    <div class="col">
                    <input type="number" class ="inpt2" name ="nombre_pers" id="tpps1" placeholder="Nombre de personnes" min ="0" max ="30">
                    </div>
                </div>
                </br>
                <p class ="photo">Ajoute une photo !</p>
                    <input type="file" class="form-control jul"
                        id="avatar" name="avatar" value ="wesh"
                        accept="image/png, image/jpeg" placeholder="Partage la photo de ta recette !">

        </div>    
        <hr class="hh">    
           <h2  class="fw-light bda">Choisi tes aliments</h2> 
           <div id="prepa">
                 <div class="form-group col-md-4" id="zer">
                <select id="inputState" class="form-control" onchange="Charge_Combo()">
                    <option  selected>Choisis une famille...</option>
                    <?php include_once "../models/famille.php";
                    $famille = new Famille();
                    $res= $famille->get_all();
                    if($res!==null){
                        while($row = $res->fetch(PDO::FETCH_ASSOC)){

                            extract($row); ?>
    
                        <option value="<?php echo $id_famille?>"><?php echo $nom?></option>

                        <?php                        
                    }
                }
                ?>
                </select>
            
                </div>
             
                <select id="ingredient" class="sister"> 
                    <option value = "0">Choisissez votre aliment</option>  
                </select>
                
                <button type="button" class="btn btn-primary mb-2" onclick="add_alim()" id ="btnEnzo">Ajouter à la liste</button>
                
                    
                <input type="number" class ="cati" id="inputZip" name ="gram" placeholder="Dosage en gramme" min ="1" max ="1000">
                <input type="number"  class ="cati2"  id="putt"  name ="quantite" placeholder="Quantite" min ="1" max ="30">
                    
                <ul class="list-group lista" id="liste">
                </ul>
                <div id="error"></div>

            </div> 
            </br>  
            <h2  class="fw-light bda">Description de la recette</h2>
            <div id="prepa">
            <div class="form-group">
            <p class ="photo">Description de la recette</p>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" name="descr"></textarea>
            </div>
            </br>
            <button type="button" class="btn btn-success mb-2 btn-enzo" onclick="recupListeingredient()">Ajoute ta recette !</button>  
            </br>
            <div id ="err"></div> 
            </div> 
        </form>
    </div>
    <p id="user"><?php echo $_SESSION['id_user']?></p>
    <?php
    include("footer.php");
    ?>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>    
<script src="ingre.js"></script>
</body>
</html>