
<header class="p-3 bg-dark text-white" id="fond">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="accueil.php" class="nav-link px-2 text-secondary">Accueil</a></li>
          <li><a href="ListeRecettes.php" class="nav-link px-2 text-white">Recettes</a></li>
          <li><a href="FAQ.php" class="nav-link px-2 text-white">FAQ</a></li>
        </ul>

        <?php require_once "../user/authe.php";

        // si n'est pas connecté
          if(!is_connected()){

            ?>
    
            <div class="text-end">
            <a class="btn btn-primary my-2" href="log.php">Se connecter</a>
            <a class="btn btn-warning my-2" href="SignUp.php">Inscription</a>
            </div>
    
            <?php 
            }

        // si connecté en tant qu'admin 
        if (is_connected() && $_SESSION['admin'] == 1){

        ?>
          <div class="text-end">
            <!-- l'admin ne va pas sur le même dashboard que l'user car il n'a pas de recettes => retour vers l'accueil -->
            <a class="btn btn-danger my-2" href ="accueil.php"><?php echo $_SESSION['firstname'].' '.$_SESSION['name']?></a></label>
            <a class="btn btn-warning my-2" href="deconnexion.php">Deconnexion</a></button>
          </div>

          <?php
        }


        // si connecté en tant qu'user simple
        if (is_connected() && $_SESSION['admin'] == 0){
       ?>
          <div class="text-end">
            <a class="btn btn-primary my-2" href ="dashboard.php"><?php echo $_SESSION['firstname'].' '. $_SESSION['name'] ?></a></label>
            <a class="btn btn-warning my-2" href="deconnexion.php">Deconnexion</a></button>
          </div>

          <?php
        }
        


        ?>
      </div>
    </div>
  </header>