<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Highpy Meal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="css/accueil1.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/ficherecette.scss" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">
    
    <!-- Bootstrap core CSS -->
    <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    
     <link href="css/cree.css" rel="stylesheet">
    <meta name="theme-color" content="#7952b3">

    </head>

    <body class = "bod" id="myCanvas">

        
    <?php
        include("header.php");
    ?>
    <div class="row">
      <div class="col-4">
        <a class="btn btn-primary btn-sm" href="http://localhost/HighpyMeal/front/dashboard.php">Retour</a>
      </div>
    </div>

    <div class="row">
      <?php 
        require_once "../models/recette.php";
        $recette = new Recettes();
        $id_recette = $_GET['id_recette'];
        $result = $recette->lire_recette_selected_user($id_recette);
        $result_ = $recette->lire_ingredients_recette_selected($id_recette);
        $recette_ = $result->fetch(PDO::FETCH_ASSOC);        
        ?>
          <div class="card row" id="recette">
            <div class="header">
                <div class="icon">
                <img class="card-img-top" src="export.php?id_recette=<?php echo ($id_recette)?>" alt="Card image cap">
                </div>
            </div>
            <div class="text">
                <h1 class="food" style="color: black; text-align: center">                             
                  <?php echo($recette_['nom'])?>
                </h1>
                <p><i class="fa fa-clock" style="margin-right: 1rem"></i><?php echo($recette_['temps_preparation'])?></p>
                <p><i class="fa fa-users" style="margin-right: 1rem"></i><?php echo($recette_['Nombre_de_personnes'])?></p> 
                <p><i class="fa fa-receipt" style="margin-right: 1rem"></i>Étapes:</p>
                <p style="margin-left: 2rem"><?php echo($recette_['descriptif'])?></p>
                <p><i class="fa fa-shopping-basket" style="margin-right: 1rem"></i>Ingrédients:</p>
                <ul>
                  <?php      
                     $tableau_ingredients = array();       
                      while($ingredient =  $result_->fetch(PDO::FETCH_ASSOC)){
                        $result__ = $recette->lire_nom_ingredient_recette($ingredient['id_ingredient']);
                        $nom_ingredient = $result__->fetch(PDO::FETCH_ASSOC);
                        array_push($tableau_ingredients, $nom_ingredient['nom']); 
                       
                        ?><li>
                          <?php 
                            echo($nom_ingredient['nom']);                               
                          ?>
                        </li> 
                  <?php 
                    }
                    $liste_tableau_ingredients = implode(",", $tableau_ingredients); 
                  ?>
                </ul>                            
              </div>                     
            </div>
          <div class="d-flex flex-wrap align-items-center justify-content-around">
          <div >                            
            <button 
              class="btn btn-info" 
              onclick="kendoPDF()">
                  PDF
            </button>
          </div>

          <div >           
            <input id="nom_recette" type="hidden" value="<?php echo($recette_['nom'])?>"/>
            <input id="parts_recette" type="hidden" value="<?php echo($recette_['Nombre_de_personnes'])?>"/>
            <input id="ingredients" type="hidden" value="<?php echo($liste_tableau_ingredients)?>"/>
            <input id="descriptif_recette" type="hidden" value="<?php echo($recette_['descriptif'])?>"/>
            
            <a href="javascript:(function()%7BSendLinkByMail()%3B%7D)()%3B">
            <img style="width: 4rem; height: 4rem" src="http://png-2.findicons.com/files/icons/573/must_have/48/mail.png">
            </a>
          </div>

          </div>
        </div>
      </div>

    <?php
    include("footer.php");
    ?>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>   
<script src="./clicPDF.js"></script>
<script src="./clicMail.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
<script src="https://smtpjs.com/v3/smtp.js"></script>
</body>
</html>