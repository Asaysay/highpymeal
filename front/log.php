<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Highpy Meal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="signin.css" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">
    <!-- Bootstrap core CSS -->
    <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="css/accueil1.css" rel="stylesheet">
    
    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">
        <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">
       
    </head>

    <body class="text-center">
        <?php
        include("header.php");
        ?> 
    
    
    <?php
    require_once "../user/authe.php"; // on verifie que l'on a les information de connexion
    if(!empty($_POST['email']) && !empty($_POST['password'])){
        $result =  check_user($_POST['email'],$_POST['password']);       
        // si on trouve  pas
        if($result===null){ ?>

        <div class="alert alert-danger">
                Mot de passe ou indentifiant incorrect
        </div>

    <?php
    // sinon on lance la session et on renvoie a la page d'accueil
        }else{

        // $id_recette_selected = ""
        
        $_SESSION['connecte'] = 1;
        $_SESSION['name']= $result['nom'];
        $_SESSION['firstname']=$result['prenom'];
        $_SESSION['id_user']=$result['id_user'];
        $_SESSION['admin']=$result['admin'];

        if ($result['admin']===true){
            $_SESSION['admin']==="1";
        }
        }
     }
    require_once "../user/authe.php";
        //on regarde en arrivant si le user est deja connecté et si oui on le renvoie a l'accueil
        if(is_connected()){
            header("Location: accueil.php");
            exit();
        }
    ?>


    <main class="form-signin">
    <form action="" method="post">
        <h1 class="h3 mb-3 fw-normal">Connectez vous</h1>

        <div class="form-floa" id="floatingInput" placeholder="name@example.com">
            <input type="email" name ="email" id="floatingPassword" placeholder="email">
        </div>
        <div class="form-floating">
        <input type="password" name ="password" id="floatingPassword" placeholder="Password">
        
        </div>

        <div class="checkbox mb-3">
        </div>
        <button class ="btn btn-primary" type="submit">Se connecter</button>
        <p class="mt-5 mb-3 text-muted" >&copy; 2021-2022</p>
    </form>
    </main>

    <?php
        include("footer.php");
    ?>
    </body>
</html>


    