<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Highpy Meal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="css/accueil1.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/signup.css" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">

    <!-- Bootstrap core CSS -->
    <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">

    </head>
    <body>
    <?php
        include("header.php");
    ?>
    
  
        <div class="enzo">
           <div class="dash">
               <h1 class="fw-light bdu"> Tableau de bord de <?php echo $_SESSION["firstname"]?></h1>
               <a class="btn btn-primary my-2" id ="bttn" href="create.php">Poste une recette</a>
               
            </div>
            <h1 class="fw-light bd">Toutes tes recettes</h1>
            <hr>
</br>
           
           <div class="carte">
           <?php 
                require_once "../models/recette.php";
                $recette = new Recettes();

                $result = $recette->lire_rec_user($_SESSION['id_user']);

                if ($result!==false){
                    while($row = $result->fetch(PDO::FETCH_ASSOC)){
                        extract($row);                        
                        ?>
                         <div class="card rosaa" style="width: 18rem;">
                            <img class="card-img-top" src="export.php?id_recette=<?php echo $id_recette?>" alt="Card image cap">
                            <div class="card-body">
                            <h5 class="card-title"><?php echo $nom?></h5>
                                <a class="btn btn-info my-2" href="marecette.php?id_recette=<?php echo $id_recette?>">Consulte ta recette</a></button>
                            </div>
                        </div>

                  <?php  }
                }else{
           echo 
               ' <div class="card bg-dark text-white"  id ="carta">
                    <img class="card-img" src="images/rea.jpg" alt="Card image">
                    <div class="card-img-overlay">
                        <h5 class="card-title">Pas encore de recettes </h5>
                        <a class="btn btn-info my-2" href="../view/create.php">Créé ta recette</a></button>
                    </div>
                </div> ';   
            }
           ?>         
           </div>
    </div>


<?php
    include("footer.php");
?>
<script src="banniere.js"></script>
</body>
</html>