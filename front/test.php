<?php
include_once "../models/ingredient.php";

$ingre = new Ingredient();

if(!empty($_POST)){
    $res = $ingre-> get_da_one($_POST['val']);
    $num = $res->rowCount();
  
    if ($num > 0){
        $tabRecette=[];
        while($row = $res->fetch(PDO::FETCH_ASSOC)){

            extract($row);
            $produit = [
                "id_ingredient" => $id_ingredient,
                "nom" => $nom
            ];
            
            array_push($tabRecette,$produit);
        }
    }

   echo json_encode($tabRecette);
}
?>