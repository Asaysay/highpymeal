
  function SendLinkByMail() {
    var nom_recette = document.getElementById('nom_recette').value;   
    var subject= "Check cette recette de ";
    subject += nom_recette;
    subject += "!";
    var parts_recette = document.getElementById('parts_recette').value;
    var ingredients = document.getElementById('ingredients').value;
    console.log(ingredients);
    var descriptif_recette = document.getElementById('descriptif_recette').value;
    var body = `Recette de `;
    body += nom_recette;
    body += ' \n ';
    body += 'Pour '
    body += parts_recette;
    body += ' personnes'
    body += ' \n ';
    body += 'Ingrédients:'
    body += ' \n ';
    body += ingredients;
    body += ' \n ';
    body += 'Étapes:'
    body += ' \n ';
    body += descriptif_recette;
    var uri = "mailto:?subject=";
    uri += encodeURIComponent(subject);
    uri += "&body=";
    uri += encodeURIComponent(body);
    window.open(uri);
}

