
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Highpy Meal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">
    <!-- Bootstrap core CSS -->
    <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="css/accueil1.css" rel="stylesheet">
        <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">

  </head>
  <body>

<?php
require_once "../user/authe.php";


include("header.php");
?>   

<!-- non connecté -->
<?php if(!is_connected()) {
?>

<main>

  <section class="py-5 text-center container" >
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
      <h1 class="fw-light">Highpy Meal</h1>
        <p class="lead text-muted" id="bjr">Les meilleures idées de recettes à base de CBD pour des moments à partager sans se ruiner la santé (ou presque).</p>
        <p>
            <a href="SignUp.php" class="btn btn-primary my-2">Inscris toi !</a>
            <a href="SignUp.php" class="btn btn-secondary my-2">Devenir premium</a>
        </p>
      </div>
    </div>
  </section>
  <div class="album py-5 bg-light" id ="fond">

              </div>
            </div>
          </div>
        </div> 
        <?php
      }
      ?>      
</main>

<?php
// } 
?>


<!-- connecté en mode user simple -->
<?php if(is_connected() && $_SESSION['admin'] == 0) {
  ?>

<main>

  <section class="py-5 text-center container" >
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
      <h1 class="fw-light">Highpy Meal</h1>
        <p class="lead text-muted" id="bjr">Les meilleures idées de recettes à base de CBD pour des moments à partager sans se ruiner la santé (ou presque).</p>
        <p>
            <a href="dashboard.php" class="btn btn-primary my-2">Consulte tes recettes</a>
            <a href="create.php" class="btn btn-secondary my-2">Poste une recette !</a>
        </p>
      </div>
    </div>
  </section>
 
   </main>
<?php
 } 

// connecté en mode admin
if(is_connected() && $_SESSION['admin'] == 1) {
?>

<main>

  <section class="py-5 text-center container" >
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
      <h1 class="fw-light">Bienvenue Administrateur <?php echo($_SESSION['firstname'].' '.$_SESSION['name']) ?></h1>
        <p class="lead text-muted" id="bjr">Vous pourrez gérer ici les utilisateurs ainsi que les demandes d'inscription en attente.</p>
        <p>
            <a href="gestion_users.php" class="btn btn-primary my-2">Utilisateurs</a>
            <a href="gestion_activation_users.php" class="btn btn-secondary my-2">Inscriptions en attente</a>
        </p>
      </div>
    </div>
  </section>
  </main>
<?php
}


      //  toujours affiché
      include("footer.php");
?>
</body>
</html>


    