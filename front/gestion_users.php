<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Highpy Meal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="css/accueil1.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/signup.css" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">

    <!-- Bootstrap core CSS -->
    <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">

    </head>
    <body>
    <?php
        include("header.php");
    ?>

        <h1 class="dashboardtitle titi">Gestion des utilisateurs</h1>
        <hr>
        <div class="getuser">
            <a class="btn btn-primary btn-sm" href="http://localhost/HighpyMeal/front/accueil.php">Retour à l'accueil</a>
        
    
               <div class="row">
                </div>
                <?php 
                require_once "../models/user.php";
                $users = new Users();
                $result = $users->getAllUsersActifs();
                if ($result!==false){
                    ?>  
                    <table>
                        <th>Nom</th> 
                        <th>Prénom</th>
                        <th></th>
                    <?php
                    while($row = $result->fetch(PDO::FETCH_ASSOC)){

                        extract($row); ?>

                            <tr id="row_user">
                                <td>
                                    <?php 
                                    echo($row['nom']); 
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                    echo($row['prenom']);
                                    ?>
                                </td>
                                <td>
                                 <button class="btn btn-danger" onclick="deleteUser(<?php echo($row['id_user'])?>)">X</button>
                                </td>
                            </tr>                     
                    <?php 
                     } 
                     ?>
                  </table>
                <?php
                }else{
           echo 
               "<h4>Pas d'utilisateurs</h4>";   
            }
           ?> 
           </div>        
<?php
    include("footer.php");
?>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>    
<script src="gestion_users.js"></script>
</body>
</html>